# PayMasterDemo

Здравствуйте! Это **демонстративная** версия одного из моих проектов. Часть кода и данных скрыта по желанию работодателя. Тут вы можете ознакомиться с моим стилем написания кода и с частью моих навыков. **Сам проект лежит на ветке master**

### О проекте:


Это веб сервис для оплаты уроков. Он позволяет родителям или ученикам оплачивать необходимые уроки, а менеджерам контролировать все платежи. 

Сам сервис глобально можно разбить на три части: 
* Телеграм бот в роли клиентского приложения для плательщиков и менеджеров
* Celery для фонового и регулярного заполнения гугл таблиц данными из БД. Таблицы нужны для отслеживания платежей менеджерами
* Бэк на Django для обработки платежей и записи их в БД

*Стек технологий:*

* Python
* Django Rest Framework
* PostgreSQL
* Docker
* Nginx
* aiogram и другие асинхронные библиотеки
* Celery
* celery-beat
* pygsheets
